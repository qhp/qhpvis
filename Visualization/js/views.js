DocumentView = Backbone.View.extend({  
	tagName : "div",  
	className: "document well",
	initialize: function() {
		this.model.bind('change', this.render, this);
		this.model.bind('destroy', this.remove, this);
    },
    template: [_.template($("#template-document-newspapers").html()), _.template($("#template-document-wikipedia").html()), _.template($("#template-document-books").html())],
	render : function() {
		if(this.model.get("_index").indexOf("kb_ww2") > -1)
		{
			this.$el.html(this.template[0](this.model.toJSON()));
		}
		else
		{
			if(this.model.get("_index").indexOf("wikipedia") > -1)
			{
				this.$el.html(this.template[1](this.model.toJSON()));
			}
			else
			{
				this.$el.html(this.template[2](this.model.toJSON()));
			}
		}
		return this;
	}  
});    

FacetView = Backbone.View.extend({  
	tagName : "div",  
	className: "facet",
	initialize: function() {
		this.model.bind('change', this.render, this);
		this.model.bind('destroy', this.remove, this);
    },
    template: _.template($("#template-facet").html()),
    hit_template: _.template($("#template-facet-hit").html()),
	render : function() {
		this.$el.html(this.template(this.model.toJSON()));
		var field = this.model.get("field");
		var target = this.$el.children("#drop-" + field);
		var hits = this.model.get("hits");
		if(hits) _.each(hits[hits["_type"]], _.bind(function(hit) {
			target.append(this.hit_template({"hit": hit, "field": field}));
		}, this));

		return this;
	}  
});
// TODO: Fill drop list in FacetView on query