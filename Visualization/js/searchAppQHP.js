var SearchApp = Backbone.View.extend({
	initialize: function(options) {
		// Bind all functions that uses 'this' as the current object
		_.bindAll(this, 'render', 'addDocument', 'addDocumentWiki', 'addDocumentBooks', 'initVisualSearch');
		this.placeholders = options.placeholders;
		this.models = options.collection.models;
		this.initVisualSearch();
		this.itemList = [];
		this.suggestList = { Newspapers : { article_dc_identifier_resolver: "Artikel ID resolver", 
											article_dc_subject: "Onderwerp",
											article_dcx_recordIdentifier: "Artikel Record ID", 
											identifier: "ID", 
											paper_dc_identifier: "Krant ID",
											paper_dc_identifier_resolver: "Krant ID resolver", 
											paper_dc_language: "Taal", 
											paper_dc_publisher: "Uitgever",
											paper_dc_source: "Bron", 
											paper_dcterms_alternative: "Alternatief voor", 
											paper_dcterms_isVersionOf: "Is versie van",
											paper_dcterms_issued: "Termen toegekend", 
											paper_dcterms_spatial: "Verspreidingsgebied", 
											paper_dcterms_spatial_creation: "Plaats van uitgave",
											paper_dcterms_temporal: "Tijdsaanduiding", 
											paper_dcx_issuenumber: "Uitgiftenummer", 
											paper_dcx_recordIdentifier: "Krant Record ID",
											paper_dcx_volume: "Volume", 
											paper_ddd_yearsDigitized: "Jaren gedigitaliseerd",
											text_content: "text"
										},
							Wikipedia : { 	category: "Categorie",
											text: "text",
											title: "TitelWiki",
											link: "Link"
										},
							Books : {	book_description: "Beschrijving boek",
										book_title: "Titel boek",
										content: "text",
										coverage: "Dekking",
										format: "Data type"
									}
						};
		
		/* Newspapers */
		this.modelNewspapers = options.collection.models[0];
		this.modelNewspapers.bind("change:resultsNewspapers", this.render, this);
		this.collectionNewspapers = this.modelNewspapers.get("hits");
		this.collectionNewspapers.bind("add", this.addDocument, this);
		this.collectionNewspapers.bind("reset", function() { this.collectionNewspapers.each(this.addDocument); }, this);
		
		var detailshtml = '<div id="results-headerNewspapers"></div>';
		detailshtml += '<div class="detailcategory" id="hits_newspapers"></div>';
		this.placeholders[0].html(detailshtml);
		
		this.modelNewspapers.bind("change:query", function() {
			var progress = this.$el.children('#results-headerNewspapers').html($('#template-progress').html());
        	progress.hide().fadeIn(300);
        	this.$el.children('.detailcategory').html('');
		}, this);
		
		/* Wikipedia */
		this.modelWikipedia = options.collection.models[1];
		this.modelWikipedia.bind("change:resultsWikipedia", this.render, this);
		this.collectionWikipedia = this.modelWikipedia.get("hits");
		this.collectionWikipedia.bind("add", this.addDocumentWiki, this);
		this.collectionWikipedia.bind("reset", function() { this.collectionWikipedia.each(this.addDocumentWiki); }, this);
		
		var detailshtml = '<div id="results-headerWikipedia"></div>';
		detailshtml += '<div class="detailcategory" id="hits_wikipedia"></div>';
		this.placeholders[1].html(detailshtml);
		
		this.modelWikipedia.bind("change:query", function() {
			var progress = this.$el.children('#results-headerWikipedia').html($('#template-progress').html());
        	progress.hide().fadeIn(300);
        	this.$el.children('.detailcategory').html('');
		}, this);
		
		/* Books */
		this.modelBooks = options.collection.models[2];
		this.modelBooks.bind("change:resultsBooks", this.render, this);
		this.collectionBooks = this.modelBooks.get("hits");
		this.collectionBooks.bind("add", this.addDocumentBooks, this);
		this.collectionBooks.bind("reset", function() { this.collectionBooks.each(this.addDocumentBooks); }, this);
		
		var detailshtml = '<div id="results-headerBooks"></div>';
		detailshtml += '<div class="detailcategory" id="hits_books"></div>';
		this.placeholders[2].html(detailshtml);
		
		this.modelBooks.bind("change:query", function() {
			var progress = this.$el.children('#results-headerBooks').html($('#template-progress').html());
        	progress.hide().fadeIn(300);
        	this.$el.children('.detailcategory').html('');
		}, this);
		
	},
	initVisualSearch: function() {
		this.visualSearch = VS.init({
			container : $('.visual_search'),
		    query     : '',
		    callbacks : {
				search: _.bind(function(query, searchCollection) {// alert("SearchAppQHP: search callback");
		    		// Build the query based on the input Facets
					var query_stringNewspapers = [];
					var query_stringWikipedia = [];
					var query_stringBooks = [];
					var fieldName = "";
					for (var i=0; i < searchCollection.models.length; i++)
					{
						fieldNiceDisplayName = searchCollection.models[i].attributes.category;
						searchValue = searchCollection.models[i].attributes.value;
						if (this.valueInList(this.suggestList["Newspapers"], fieldNiceDisplayName)["doesExist"] == true)
						{
							fieldName = this.valueInList(this.suggestList["Newspapers"], fieldNiceDisplayName)["key"];
							var newspaperEntry = {};
							newspaperEntry[fieldName] = searchValue;
							query_stringNewspapers.push({"match": newspaperEntry});
						}
						
						if (this.valueInList(this.suggestList["Wikipedia"], fieldNiceDisplayName)["doesExist"] == true)
						{
							fieldName = this.valueInList(this.suggestList["Wikipedia"], fieldNiceDisplayName)["key"];
							var wikipediaEntry = {};
							wikipediaEntry[fieldName] = searchValue;
							query_stringWikipedia.push({"match": wikipediaEntry});
						}
						
						if (this.valueInList(this.suggestList["Books"], fieldNiceDisplayName)["doesExist"] == true)
						{
							fieldName = this.valueInList(this.suggestList["Books"], fieldNiceDisplayName)["key"];
							var bookEntry = {};
							bookEntry[fieldName] = searchValue;
							query_stringBooks.push({"match": bookEntry});
						}
					}
					
					var queryNewspapers = {"match_all": {}};
					var queryWikipedia = {"match_all": {}};
					var queryBooks = {"match_all": {}};
					
					if(query_stringNewspapers.length)
					{
						queryNewspapers = {"bool":{"must":query_stringNewspapers}};
					}
					
					if(query_stringWikipedia.length)
					{
						queryWikipedia = {"bool":{"must":query_stringWikipedia}};
					}
					
					if(query_stringBooks.length)
					{
						queryBooks = {"bool":{"must":query_stringBooks}};
					}
					
					this.modelWikipedia.set("query", queryWikipedia);
					this.modelBooks.set("query", queryBooks);
		        	this.modelNewspapers.set("query", queryNewspapers);
					
		        }, this),
		        facetMatches : _.bind(function(callback) {
					this.itemList = [];
					for (var entry in this.suggestList)
					{
						this.addToQACList(this.suggestList[entry], entry);
					}
					callback(this.itemList);					
		        }, this),
		        valueMatches : _.bind(function(facet, searchTerm, callback) {		
					value = facet;		
					searchTerm = searchTerm + ".*";
					var query = "";
					
					if (this.valueInList(this.suggestList["Newspapers"], value)["doesExist"] == true)
					{
						key = this.valueInList(this.suggestList["Newspapers"], value)["key"];
						query = { "query": {"query_string": { "query": "*"}}, "size": 0, "aggregations": { "suggest": { "terms": { "field": key, "include": searchTerm, "size": 10 }}}};
						this.modelNewspapers.getData(this.modelNewspapers.get("search_url") + "?search_type=count&x=3", query, function(data) {
							callback($.map(data.aggregations.suggest.buckets, function(d) { return d.key; }));
						});
					}
					
					if (this.valueInList(this.suggestList["Wikipedia"], value)["doesExist"] == true)
					{
						key = this.valueInList(this.suggestList["Wikipedia"], value)["key"];
						query = { "query": {"query_string": { "query": "*"}}, "size": 0, "aggregations": { "suggest": { "terms": { "field": key, "include": searchTerm, "size": 10 }}}};
						this.modelWikipedia.getData(this.modelWikipedia.get("search_url") + "?search_type=count&x=3", query, function(data) {
							callback($.map(data.aggregations.suggest.buckets, function(d) { return d.key; }));
						});
					}
					
					if (this.valueInList(this.suggestList["Books"], value)["doesExist"] == true)
					{
						key = this.valueInList(this.suggestList["Books"], value)["key"];
						query = { "query": {"query_string": { "query": "*"}}, "size": 0, "aggregations": { "suggest": { "terms": { "field": key, "include": searchTerm, "size": 10 }}}};
						this.modelBooks.getData(this.modelBooks.get("search_url") + "?search_type=count&x=3", query, function(data) {
							callback($.map(data.aggregations.suggest.buckets, function(d) { return d.key; }));
						});
					}
					
		        }, this)
			}
		});
	},
	template: [_.template($("#template-resultsNewspapers").html()), _.template($("#template-resultsWikipedia").html()), _.template($("#template-resultsBooks").html())],
	render : function() {
		
		var results_newspapers = searchApp.models[0].get('resultsNewspapers'); 
		if (results_newspapers != undefined) {
			this.placeholders[0].children("#results-headerNewspapers").html("<em>Er zijn " + results_newspapers.hits.total + " kranten die het noemen in deze periode.</em><br /><br />");
		}
		
		var results_wikipedia = searchApp.models[1].get('resultsWikipedia');
		if (results_wikipedia != undefined){
			this.placeholders[1].children("#results-headerWikipedia").html("<em>Er zijn " + results_wikipedia.hits.total + " Wikipedia artikelen die het noemen in deze periode.</em><br /><br />");
		}
		
		var results_books = searchApp.models[2].get('resultsBooks'); 
		if (results_books != undefined) {
			this.placeholders[2].children("#results-headerBooks").html("<em>Er zijn " + results_books.hits.total + " boeken die het noemen in deze periode.</em><br /><br />");
		}
		
		return this;
	},
	addDocument: function(hit) {
		var view = new DocumentView({model: hit});
		var target = "#hits_newspapers";
		this.placeholders[0].children(target).append(view.render().el);
	},
	addDocumentWiki: function(hit) {
		var view = new DocumentView({model: hit});
		var target = "#hits_wikipedia";
		this.placeholders[1].children(target).append(view.render().el);
	},
	addDocumentBooks: function(hit) {
		var view = new DocumentView({model: hit});
		var target = "#hits_books";
		this.placeholders[2].children(target).append(view.render().el);
	},
	addToQuery: function(field, value) {
		var query = this.visualSearch.searchBox.value();
		var newQuery = query + ' ' + field + ': "' + value + '"';
		this.visualSearch.searchBox.value(newQuery);
		this.visualSearch.options.callbacks.search(newQuery, this.visualSearch.searchQuery);
	},
	addToQACList: function(itemList, category){
		for (var item in itemList)
		{
			var newItem = {};
			newItem["label"] = itemList[item];
			newItem["category"] = category;
			this.itemList.push(newItem);
		}
	},
	valueInList: function(itemList, value){
		var result = {};
		result["doesExist"] = false;
		result["key"] = null;
		for (var key in itemList)
		{
			if(itemList[key] == value)
			{
				result["doesExist"] = true;
				result["key"] = key;
				return result;
			}
		}
		
		return result;
	}
});