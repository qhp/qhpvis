// Here we define the models used for the application.
// Each model extends [Backbone.js' Model](http://backbonejs.org/#Model).
// * * *

// We define a `Facet`, that is based on a field in the collection of documents and a collection of these.
var Facet = Backbone.Model.extend({});
var FacetCollection = Backbone.Collection.extend({model: Facet});

// `ElasticSearchModel` contains the logics to work with [ElasticSearch](http://www.elasticsearch.org/).
var ElasticSearchModel = Backbone.Model.extend({
	// The `ElasticSearchModel` contains three default fields:
	//
	// * `facets` describing the fields in the document collection. 
	// * `query` is a dictionary in ElasticSearch's 
	//   [QueryDSL](http://www.elasticsearch.org/guide/reference/query-dsl/) 
	// * `hits`, a collection of documents.
	defaults: function() { return {
		"facets": new FacetCollection,
		"query": {"match_all": {}},
		"hits": new ElasticSearchHits
	}; },
	// `initialize` gets called when a new model is created.
	initialize: function() {
		// Ensure that `this` is bound to the `ElasticSearchModel` for `newQuery`.
		_.bindAll(this, "newQuery");
		// Ensure that the default values are set.
		if(!this.get("facets")) this.set("facets", this.defaults.facets);
		if(!this.get("query")) 
		{
			this.models[0].set("query", this.defaults.query);
			this.models[1].set("query", this.defaults.query);
			this.models[2].set("query", this.defaults.query);
		}
		if(!this.get("hits")) this.set("hits", this.defaults.hits);
		// Bind the change events for both query and results.
		this.bind("change:resultsNewspapers", this.newResults, this);
		this.bind("change:resultsWikipedia", this.newResults, this);
		this.bind("change:resultsBooks", this.newResults, this);
	   	this.bind("change:query", this.newQuery, this);
	   	this.bind("change:pageNumber", this.newQuery, this);
	},
	// `newQuery` event is fired when the query changes.
	newQuery: function() {
		// Create a dictionary containing the query.
		var query;
		var titleContent, textContent, dateFieldName, dateFieldValue;
		if(this.get("search_url").indexOf("kb_ww2") > -1)
		{
			titleContent = this.get("searchTitle");
			textContent = this.get("searchText");
			dateFieldName = this.get("searchDateTitle");
			dateFieldValue = this.get("searchDateValue");
			if(dateFieldValue != undefined) { dateFieldValue = dateFieldValue[0]; } else { dateFieldValue = "1933-01-01"; }
			query = {"from":(this.get('pageNumber') - 1) * 10, "size":10, "query": {"function_score": {"functions": [{ "exp": { "paper_dc_date": { "origin": dateFieldValue, "scale": "365d", "decay": "0.6" }}}], "query":{"more_like_this": {"like_text":textContent, "min_term_freq" : 1, "min_doc_freq" : 1, "max_query_terms": 12 }}, "score_mode": "multiply" }}, "highlight":{"pre_tags":["<span style='color:red'>"],post_tags:["</span>"], "fields":{"text_content":{}, "article_dc_title":{}}}};
		}
		else
		{
			if(this.get("search_url").indexOf("wikipedia") > -1)
			{
				titleContent = this.get("searchTitle");
				textContent = this.get("searchText");
				dateFieldName = this.get("searchDateTitle");
				dateFieldValue = this.get("searchDateValue");
				if(dateFieldValue != undefined) { dateFieldValue = dateFieldValue[0]; } else { dateFieldValue = "1933-01-01"; }
				query = {"from":(this.get('pageNumber') - 1) * 10, "size":10, "query": {"function_score": {"functions": [{ "exp": { "days": { "origin": dateFieldValue, "scale": "365d", "decay": "0.6" }}}], "query":{"more_like_this": {"like_text":textContent, "min_term_freq" : 1, "min_doc_freq" : 1, "max_query_terms": 12 }}, "score_mode": "multiply"}}, "highlight":{"pre_tags":["<span style='color:red'>"],post_tags:["</span>"], "fields":{"plain_text":{}, "title":{}}}};
			}
			else
			{
				titleContent = this.get("searchTitle");
				textContent = this.get("searchText");
				dateFieldName = this.get("searchDateTitle");
				dateFieldValue = this.get("searchDateValue");
				if(dateFieldValue != undefined) { dateFieldValue = dateFieldValue[0]; } else { dateFieldValue = "1933-01-01"; }
				query = {"from":(this.get('pageNumber') - 1) * 10, "size":10, "query": {"function_score": {"functions": [{ "exp": { "days": { "origin": dateFieldValue, "scale": "365d", "decay": "0.6" }}}], "query":{"more_like_this": {"like_text":textContent, "min_term_freq" : 1, "min_doc_freq" : 1, "max_query_terms": 12 }}, "score_mode": "multiply"}}, "highlight":{"pre_tags":["<span style='color:red'>"],post_tags:["</span>"], "fields":{"book_title":{}, "proper_text":{}}}};
			}
		}
		// Get the results from the server and when successful set the results field.
       	this.getData(this.get("search_url"), query,
        	_.bind(function(data) {
				 if(this.get("search_url").indexOf("kb_ww2") > -1)
				 {
					this.set("resultsNewspapers", data);
				 }
				 if(this.get("search_url").indexOf("wikipedia") > -1)
				 {
					this.set("resultsWikipedia", data);
				 }
				 if(this.get("search_url").indexOf("books") > -1)
				 {
					this.set("resultsBooks", data);
				 }
				 
        	}, this)
        );
        // Seperately get the facets. This is inefficient, but makes the app feel more responsive.
        query["facets"] = {};
        // Include each facets in the ElasticSearch query.
        this.get("facets").each(function(facet) {
			query["facets"][facet.get("field")] = {"terms": {"field": facet.get("field") }};
        });
        // Get the data with a more efficient *count* type query, storing each hit for 
        // a facet in the appropriate `Facet`.
		this.getData(this.get("search_url"), query,
        	_.bind(function(data) {
        		_.each(data["facets"], function(facet, field) {
        			this.facets.where({"field": field})[0].set("hits", facet);
        		});
        	}, this)
        );        
	},
	// `newResults` events gets fired when the results are in. Just store the hits field from this for now.
	newResults: function() { 
		if(this.get("search_url").indexOf("kb_ww2") > -1)
		{
			this.get("hits").reset(this.get("resultsNewspapers").hits.hits); 
		}
		if(this.get("search_url").indexOf("wikipedia") > -1)
		{
			this.get("hits").reset(this.get("resultsWikipedia").hits.hits); 
		}
		if(this.get("search_url").indexOf("books") > -1)
		{
			this.get("hits").reset(this.get("resultsBooks").hits.hits); 
		}
	},
	// `getData` takes care of communication with search engine, taking care of JSONifying the query.
	getData: function(url, query, callback) {
		if(url != undefined) {
			if (query != undefined) {
				$.ajax({
					url: url, 
					type: 'POST', 
					data: JSON.stringify(query),
					dataType : 'json',  
					processData: false, 
					success: callback, 
					error: function(xhr, message, error) {
						console.error("Error while loading data from ElasticSearch", message);
						throw(error);
					}
				});
			}
		}
	}
});	

// `PersistingCollection` is a special type of collection that ensures that all models in 
// the collection get destroyed if they are removed from the collection.
var PersistingCollection = Backbone.Collection.extend({
	initialize: function() {
		_.bindAll(this, "bindReset");
		this.bind("remove", function(model) { model.destroy(); });
		this.bind("add", this.bindReset, this);
		this.bind("reset", function() { this.each(this.bindReset); }, this);
	},
	// `bindReset` will bind the reset event to the destroy function of a model
	bindReset: function(model) {
		this.bind("reset", function() { this.destroy(); }, model); 
	}
});

// An `ElasticSearchHit` is a document in the hits that result from the query and these are 
// stored in a `PersistingCollection`.
var ElasticSearchHit = Backbone.Model.extend({});
var ElasticSearchHits = PersistingCollection.extend({model: ElasticSearchHit});

// An `FacetHit` is a value for a view, contains just a term and a count
// and these stored in a `PersistingCollection`.
var FacetHit = Backbone.Model.extend({});
var FacetHits = PersistingCollection.extend({model: FacetHit});