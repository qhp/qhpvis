var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
      host: 'zookst18.science.uva.nl:8009',
        log: 'trace'
});


client.ping({
    // ping usually has a 100ms timeout
    requestTimeout: 1000,

    // undocumented params are appended to the query string
    hello: "elasticsearch!"
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
});

client.search({
    q: 'koco'
}).then(function (body) {
    var hits = body.hits.hits;
}, function (error) {
    console.trace(error.message);
});
