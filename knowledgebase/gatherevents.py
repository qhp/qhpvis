#currently not used

#For querying all events from wikipedia
from SPARQLWrapper import SPARQLWrapper, JSON
from time import sleep
 
sparql = SPARQLWrapper("http://dbpedia-live.openlinksw.com/sparql")
sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    select *
    where { 
        ?concept rdf:type dbpedia-owl:Event .
        ?s dbpedia-owl:abstract ?abstract
        }
""")
#standard example
#sparql.setQuery("""
#PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#SELECT ?label
#WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
#""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
  
for result in results["results"]["bindings"]:
    print(result)
    sleep(3)
    print '\n\n'
