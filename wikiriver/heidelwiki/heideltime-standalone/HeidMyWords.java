import java.io.IOException;
import static java.lang.System.out;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;


public class HeidMyWords {
    
    public static void main(String[] args){
        try{
            //TODO:
            //paste in wikiriver
            String date = "1940";
            System.out.println("All mentions of the most common year " + date);
            //save article to parse in 'heidarticle'

            Process proc = Runtime.getRuntime().exec("./script2.sh /");
            try {
                proc.waitFor();
                //Now read the file to finish
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                ////String content = new String(readAllBytes(get("out")));
                ////Document document = db.parse(content);
                Document document = db.parse(new File("out"));
                NodeList nodeList = document.getElementsByTagName("TIMEX3");

                String str = "";
                //String substr = "";

                //String finaldate = "";
//                int finaldatelen = 0;

                //assuming there won't be more than 9999 dates in any wiki article
                int numItems = nodeList.getLength();
                int bestcount = -1;
                int count = 0;
                String bestyear = "1000";
                String bestmonth = "01";
                String bestday = "01";
                System.out.println("test");
                
                HashMap<String, Integer> counters = new HashMap<String, Integer>(numItems);
                ArrayList<String> dateslist = new ArrayList<String>(numItems);

                //Find all those with years; store how often we find years
                for(int x=0,size=numItems; x<size; x++) {
                    str = nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue();
                    System.out.println(str);
                    String[] dates = str.split("-");
                    //check if a year is available (first val)
                    if(dates[0].length() == 4){
                        if(counters.containsKey(dates[0]))
                            counters.put(dates[0], counters.get(dates[0]) + 1);
                        else
                            counters.put(dates[0], 1);
                        dateslist.add(str);
                    }
                }
                //Count which year was most common
                for(Map.Entry<String, Integer> countEntry : counters.entrySet()){
                    count = countEntry.getValue();
                    if(count > bestcount){
                        bestcount = count;
                        //best year
                        bestyear = countEntry.getKey();
                    }
                }

                //We now know the best year.. vote for best month
                counters = new HashMap<String, Integer>(numItems);
                dateslist = new ArrayList<String>(numItems);
                count = 0;
                bestcount = 0;

                for(int x=0,size=numItems; x<size; x++) {
                    str = nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue();
                    String[] dates = str.split("-");
                    //check only for best year and if month is available
                    //System.out.println("\n\n");
                    //System.out.println(str);
                    //System.out.println(bestyear);
                    //System.out.println(dates.length);
                    if(dates[0].equals(bestyear) && dates.length > 1){
                        //Now check for months
                        if(counters.containsKey(dates[1]))
                            counters.put(dates[1], counters.get(dates[1]) + 1);
                        else
                            counters.put(dates[1], 1);
                      //  System.out.println(dates[1]);
                        //System.out.println(counters.get(dates[1]));
                        dateslist.add(str);
                    }
                }

                //Count which month was most common
                for(Map.Entry<String, Integer> countEntry : counters.entrySet()){
                    count = countEntry.getValue();
                    if(count > bestcount){
                        bestcount = count;
                        //best year
                 //       System.out.println("new best month");
                        bestmonth = countEntry.getKey();
                //        System.out.println(bestmonth);
                    }
                }


                //We now know the best year/month.. only day left
                counters = new HashMap<String, Integer>(numItems);
                dateslist = new ArrayList<String>(numItems);
                count = 0;
                bestcount = 0;

                for(int x=0,size=numItems; x<size; x++) {
                    str = nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue();
                    String[] dates = str.split("-");
                    //check only for best year and if month is available
                    if(dates[0].equals(bestyear) && dates.length > 2 && dates[1].equals(bestmonth)){
                        //Now check for days
                        if(counters.containsKey(dates[2]))
                            counters.put(dates[2], counters.get(dates[2]) + 1);
                        else
                            counters.put(dates[2], 1);
                        dateslist.add(str);
                    }
                }

                //Count which month was most common
                for(Map.Entry<String, Integer> countEntry : counters.entrySet()){
                    count = countEntry.getValue();
                    if(count > bestcount){
                        bestcount = count;
                        //best day
                        bestday = countEntry.getKey();
                    }
                }

                System.out.println("\nbest result: \n" + bestyear + " " + bestmonth + " " + bestday);

            } catch (Exception e) {
                System.out.println("Something went wrong.");
                System.out.println(e.getMessage());
                e.printStackTrace();
                System.exit(0);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
