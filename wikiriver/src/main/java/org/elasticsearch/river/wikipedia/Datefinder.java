package org.elasticsearch.river.wikipedia;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap; 
import java.util.Map; 

public class Datefinder {

    public static void main (String[] args) {
        List<String> stringList = new ArrayList<String>();
        stringList = datefinder("this is'45 '45a t21901ext 1945 written in 2014 2014 2014 '45 '45 '45 '45 '45 ");
        stringList = datefinder("'23 ");
        System.out.println (stringList.get(0));
        System.out.println (stringList.get(1));
    }
    
    public static List<String> datefinder(String text) {
        List<String> allMatches = new ArrayList<String>();
        Matcher m = Pattern.compile("((?<!\\d)(19|20|')\\d{2}(?!\\d))")
              .matcher(text);
        while (m.find()) {
            String word = "";
            if(m.group().substring(0,1).equals("'")){
                word = "19" + m.group().substring(1,3);
            }
            else{
                word = m.group();
            }
            allMatches.add(word);
        }
        String[] years = allMatches.toArray(new String[allMatches.size()]);
        
        List<String> results = new ArrayList<String>();
        results = countWords(years);

        return results;
    }

    private static List<String> countWords(String[] myWords) {
        Map<String , Integer> dictionary=new HashMap<String,Integer>(); 
        for(String s:myWords){
            if(dictionary.containsKey(s))
                dictionary.put(s, dictionary.get(s)+1);
            else
                dictionary.put(s, 1);
        }

        Map.Entry<String, Integer> maxEntry = null;
        Map.Entry<String, Integer> maxEntry2 = null;
        
        for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
            if (maxEntry2 == null || entry.getValue().compareTo(maxEntry2.getValue()) > 0) {
                //Is larger than entry 2
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                    //Is also larger than entry 1
                    maxEntry = entry;
                }
                else{
                    maxEntry2 = entry;
                }
            }
        }

        String year1;
        if(maxEntry == null)
            year1 = "1000-01-01";
        else
            year1 = maxEntry.getKey() + "-01-01";

        String year2;
        if(maxEntry2 == null || (maxEntry2.getValue() < 3 || (float) maxEntry2.getValue() / (float) maxEntry.getValue() < 0.3))
            year2 = "1000-01-01";
        else
            year2 = maxEntry2.getKey() + "-01-01";


        List<String> results = new ArrayList<String>();
        results.add(year1);
        results.add(year2);

        return results;
    }
}
