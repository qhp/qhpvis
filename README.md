Project Description:
=======
The Second World War is a defining event in our recent history. A huge amount of digital material has become
available to study it, ranging from newspapers published during and after the war, books about the war and, more
recently, Web pages about people, places and events that played a role. Each source has a different perspective
on what happened, depending on the medium, time and location of publication. In this project we aim to
quantify these different perspectives. For this purpose, we employ a data science pipeline for selection,
structuring, linking and visualization of WOIIrelated
material from NIOD, the National Library of the Netherlands,
and Wikipedia. With the data and visualization tools we produce, we provide insight into the volume, selection and
depth of WOIIrelated
topics across different media, times and locations.

Selection:
------
We use NIOD’s digitized reference work on WOII by Loe de Jong to select a limited selection of key events. We
link these events to pages in the Dutch Wikipedia and to newspapers of the National Library. Finally, we will use
them as seed events to extend our collection to a broader set of pages and articles that are lexically or
structurally similar.

Structuring and Linking:
------
We use the xTAS tool to detect named entities, and we will develop methods for finding related events and
representative pages in the different European Wikipedias and newspapers. The result will be a structured
knowledge base with annotations and links between pages/articles.

Visualization:
-----
We provide two insightful visualizations of the data:

(1) an exploratory overview of the volume of data on a
particular WOIIrelated
topic in the three data sources, based on the ThemeStreams tool of UvA; 

(2) an interactive
Web interface in which a user can select two articles/pages for a detailed comparison of the content.

References:
-----

[1] http://dsrc.wmprojects.nl/?page_id=696

[2] https://www.dropbox.com/s/1pnricsysqq0l8t/WWII-perspectives.pdf